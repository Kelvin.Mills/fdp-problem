public class Application {
    private Burger burger;

    public Application(int type){
        if (type == 1) burger = new BeefBurger();
        else burger = new VeggieBurger();
    }

    public Burger getBurger() {
        return burger;
    }
}

